const mongoose = require('mongoose')
// db connexion
mongoose.Promise = global.Promise
mongoose.connect('mongodb://localhost/quizz', { useNewUrlParser: true })

const db = mongoose.connection
db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', () => console.log("connected to", db.client.s.url))

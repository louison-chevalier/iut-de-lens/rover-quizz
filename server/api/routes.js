module.exports = app => {
  const quiz = require('./controllers')
  // for parsing application/x-www-form-urlencoded
  app.route('/quizzes')
    .get(quiz.listAllQuizzes)
    .post(quiz.createAQuiz)
  
  app.route('/quizzes/:quizId')
    .get(quiz.getAQuiz)
    .put(quiz.updateAQuiz)
    .delete(quiz.deleteAQuiz)
}



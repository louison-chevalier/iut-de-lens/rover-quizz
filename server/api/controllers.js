const mongoose = require('mongoose')
const Quiz = mongoose.model('Quiz')

exports.listAllQuizzes = (req, res) => {
  console.log('listAllQuizzes')
  Quiz.find({}, (err, quiz) => {
    if (err) { res.send(err) }
    console.log(quiz)
    res.json(quiz)
  })
}

exports.createAQuiz = (req, res) => {
  console.log('createAQuiz')
  let newQuiz = new Quiz(req.body)
  newQuiz.save((err, quiz) => {
    if (err) res.send(err)
    res.json(quiz)
  })
}

exports.getAQuiz = (req, res) => {
  Quiz.findById(req.params.quizId, (err, quiz) => {
    if (err) { res.send(err) }
    res.json(quiz)
  })
}

exports.updateAQuiz = (req, res) => {
  Quiz.findOneAndUpdate({ _id: req.params.quizId }, req.body, { new: true }, (err, quiz) => {
    if (err) { res.send(err) }
    res.json(quiz)
  })
}

exports.deleteAQuiz = (req, res) => {
  Quiz.remove({ _id: req.params.quizId }, (err, quiz) => {
    if (err) { res.send(err) }
    res.json({ message: 'Quiz successfully deleted' })
  })
}

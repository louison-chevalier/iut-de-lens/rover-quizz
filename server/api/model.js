'use strict'
const mongoose = require('mongoose')
const Schema = mongoose.Schema

const Quiz = new Schema({
  _id: Number,
  name: String,
  icon: String,
  keywords: [ String ],
  questions: [
    {
      question: String,
      txtAnswers: [ String ],
      imgAnswers: [ String ],
      solutions: [ String ],
      points: Number
    }
  ],
  published: Boolean,
  ownerId: Number,
  scores: []
})

module.exports = mongoose.model('Quiz', Quiz)

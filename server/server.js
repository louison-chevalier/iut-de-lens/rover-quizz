const express = require('express')
const mongoose = require('mongoose')
const Model = require('./api/model')
const bodyParser = require('body-parser')
const cors = require('cors')

const port = process.env.PORT || 8081
const app = express()

mongoose.Promise = global.Promise
mongoose.connect('mongodb://localhost/quizz', { useNewUrlParser: true })

app.use(cors())
app.use(express.static('resources'))
app.use(bodyParser.json()) // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true }))

const routes = require('./api/routes')
routes(app)
app.listen(port, () => console.log('Example app listening on port ' + port))

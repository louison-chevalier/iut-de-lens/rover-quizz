// src/routes.jsx
import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import Layout from './Layout'

import Home from './pages/Home.js'
import Quizzes from './pages/Quizzes.js'
import Quiz from './pages/Quiz'
import AddQuiz from './pages/AddQuiz'
import PutQuiz from './pages/PutQuiz'

export default function MainRouter () {
  return (
    <Router>
      <div>
        <Switch>
          <Route exact path='/' component={Home} />
          <Layout exact path='/quizzes' component={Quizzes} />
          <Layout exact path='/quiz/:id' component={Quiz} />
          <Layout exact path='/addQuiz' component={AddQuiz} />
          <Layout path='/putQuiz/:id' component={PutQuiz} />
          <Route component={() => <p>Page Not Found</p>} />
        </Switch>
      </div>
    </Router>
  )
}

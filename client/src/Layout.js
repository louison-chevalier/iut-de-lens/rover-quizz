import React, { Fragment } from 'react'
import { Link, Route } from 'react-router-dom'
import 'bulma'

const Layout = ({ component: Page, ...rest }) => {
  return (
    <Route {...rest} render={matchProps => (
      <Fragment>
        <div className='-header'>
          <div className='navbar navbar-dark bg-dark shadow-sm'>
            <div className='container d-flex justify-content-between'>
              <Link to='/quizzes' className='navbar-brand d-flex align-items-center'>
                <strong>Rover Quizzes</strong>
              </Link>
              <Link to='/addQuiz' className='navbar-brand d-flex align-items-center'>
                <strong>Nouveau Quiz</strong>
              </Link>
            </div>
          </div>
        </div>
        <main role='main'>
          <Page {...matchProps} />
        </main>
      </Fragment>
    )} />
  )
}
export default Layout

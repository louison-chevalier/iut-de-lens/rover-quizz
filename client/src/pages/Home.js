import React from 'react'
import { Link } from 'react-router-dom'
import './Home.scss'
export default function Home () {
  return (
    <header>
      <div className='container-fluid'>
        <div className='row'>
          <div className='col-md-7 order-md-2 header-background' />
          <div className='col-md-5 order-md-1 header-content'>
            <div className='header-content-text'>
              <h1>ROVER QUIZ</h1>
              <p>Votre avenir commence ici (ou à Alquines) ! Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
              <Link to={'/quizzes'}><button className='btn btn-primary' >COMMENCER MAINTENANT</button></Link>
            </div>
          </div>

        </div>
      </div>
    </header>
  )
}

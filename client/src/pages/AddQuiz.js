import React, { Component, Fragment } from 'react'
import axios from 'axios'
import { HTTP_SERVER_PORT } from '../constants'

export default class AddQuizz extends Component {
  constructor (props) {
    super(props)
    this.state = {
      keywords: [],
      name: '',
      questions: [{ question: '', txtAnswers: [], solutions: [], points: 0 }]
    }
    this.addQuestion = this.addQuestion.bind(this)
    this.handleChangeQuestion = this.handleChangeQuestion.bind(this)
    this.addTxtAnswer = this.addTxtAnswer.bind(this)
    this.handleChangeTxtAnswer = this.handleChangeTxtAnswer.bind(this)
    this.handleCheckTxtAnswer = this.handleCheckTxtAnswer.bind(this)
    this.isDone = this.isDone.bind(this)
    this.handleChangeName = this.handleChangeName.bind(this)
    this.handleChangeKeywords = this.handleChangeKeywords.bind(this)
    this.handleChangePoints = this.handleChangePoints.bind(this)
  }

  async isDone (e) {
    e.preventDefault()
    let response = await {
      _id: new Date().getTime(),
      name: this.state.name,
      keywords: this.state.keywords,
      questions: this.state.questions
    }
    await axios.post(`${HTTP_SERVER_PORT}/quizzes/`, response)
    this.props.history.push("/quizzes");
  }

  addQuestion () {
    let questions = this.state.questions
    questions.push({ question: '', txtAnswers: [], solutions: [], points: 0 })
    this.setState({ questions })
  }

  addTxtAnswer (indexQuestion) {
    let questions = this.state.questions
    questions[indexQuestion].txtAnswers.push('')
    this.setState({ questions })
  }

  handleChangeName (e) {
    let name = this.state.name
    name = e.target.value
    this.setState({ name })
  }

  handleChangeKeywords (e) {
    let keywords = this.state.keywords
    keywords = e.target.value.split(' ')
    this.setState({ keywords })
  }

  handleChangeQuestion (e, indexQuestion) {
    let questions = this.state.questions
    questions[indexQuestion].question = e.target.value
    this.setState({ questions })
  }

  handleChangePoints (e, indexQuestion) {
    let questions = this.state.questions
    questions[indexQuestion].points = e.target.value
    this.setState({ questions })
  }

  handleChangeTxtAnswer (e, indexTxtAnswer, indexQuestion) {
    let questions = this.state.questions
    questions[indexQuestion].txtAnswers[indexTxtAnswer] = e.target.value
    this.setState({ questions })
  }

  handleCheckTxtAnswer (e, indexTxtAnswer, indexQuestion) {
    indexTxtAnswer += 1
    let questions = this.state.questions
    if (e.target.checked && questions[indexQuestion].solutions.includes(indexTxtAnswer) === false) {
      questions[indexQuestion].solutions.push(indexTxtAnswer)
    } else {
      questions[indexQuestion].solutions = questions[indexQuestion].solutions.filter(rep => rep !== indexTxtAnswer)
    }
    this.setState({ questions })
  }

  render () {
    let { questions } = this.state
    return (
      <div className='container py-5'>
      <div className='row justify-content-center'>
        <form method='post' className='col-10' onSubmit={this.isDone}>
          <div className='input-group py-2'>
            <input type='text' name='name' placeholder='Nom' className='form-control' onChange={this.handleChangeName} />
          </div>
          <textarea name='keywords' id='textarea' className='form-control' cols='30' rows='4' placeholder='Write some keywords' onChange={this.handleChangeKeywords} />
          { questions.length === 0 ? '' : questions.map((question, indexQuestion) => <Question handleChangeQuestion={this.handleChangeQuestion} handleCheckTxtAnswer={this.handleCheckTxtAnswer} handleChangePoints={this.handleChangePoints} question={question} addTxtAnswer={this.addTxtAnswer} handleChangeTxtAnswer={this.handleChangeTxtAnswer} key={`question-${indexQuestion}`} indexQuestion={indexQuestion} />) }
          <div className='input-group py-2'>
            <input type='button' value='Ajouter une question' className='btn btn-primary col-12' onClick={this.addQuestion} />
          </div>
          <div className='input-group py-2'>
            <input type='submit' value='Ajouter ce quiz' className='btn btn-success col-12' />
          </div>
        </form>
      </div>
      </div>
    )
  }
}

const Question = ({ handleChangeQuestion, handleChangeTxtAnswer, handleCheckTxtAnswer, handleChangePoints, addTxtAnswer, indexQuestion, question }) => {
  return (
    <Fragment>
      <div className='input-group py-2'>
        <div className='input-group-prepend'>
          <input type='text' className='form-control' placeholder='Question' onChange={e => handleChangeQuestion(e, indexQuestion)} />
        </div>
        <select className='custom-select' onChange={e => handleChangePoints(e, indexQuestion)}>
          <option value={0}> 0 </option>
          <option value={1}> 1 </option>
          <option value={2}> 2 </option>
          <option value={3}> 3 </option>
          <option value={4}> 4 </option>
        </select>
      </div>

      { question.txtAnswers.length === 0 ? '' : question.txtAnswers.map((txtAnswer, indexTxtAnswer) => <TxtAnswer handleChangeTxtAnswer={handleChangeTxtAnswer} handleCheckTxtAnswer={handleCheckTxtAnswer} key={`question-${indexQuestion}_txtAnswer-${indexTxtAnswer}`} indexTxtAnswer={indexTxtAnswer} indexQuestion={indexQuestion} />) }
      <input type='button' value='Ajouter une réponse' onClick={() => addTxtAnswer(indexQuestion)} className='btn btn-secondary col-12' />
    </Fragment>
  )
}

const TxtAnswer = ({ indexQuestion, indexTxtAnswer, handleChangeTxtAnswer, handleCheckTxtAnswer }) => {
  return (
    <div>
      <div className='input-group mb-2 mr-sm-2'>
        <div className='input-group-prepend'>
          <div className='input-group-text'>
            <input className='' type='checkbox' name='solution' onChange={e => handleCheckTxtAnswer(e, indexTxtAnswer, indexQuestion)} />
          </div>
        </div>
        <input type='text' className='form-control' id='inlineFormInputGroupUsername2' placeholder={`TxtAnswer ${indexTxtAnswer}`} onChange={(e) => handleChangeTxtAnswer(e, indexTxtAnswer, indexQuestion)} />
      </div>
    </div>
  )
}

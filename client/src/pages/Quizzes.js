import React, { Component, Fragment } from 'react'
import { HTTP_SERVER_PORT } from '../constants'
import { Link } from 'react-router-dom'
import axios from 'axios'
import 'bulma'

export default class Quizzes extends Component {
  constructor (props) {
    super(props)
    this.state = {
      data: []
    }
    this.loadData()
  }
  async loadData () {
    console.log(HTTP_SERVER_PORT)

    const quizzes = (await axios.get(`${HTTP_SERVER_PORT}/quizzes`)).data
    this.setState({ data: quizzes })
  }

  render () {
    if (this.state.data === undefined || this.state.data.length === 0) {
      return (<Fragment> <div>Load data...</div> </Fragment>)
    } else {
      return (
        <div className='album py-5 bg-light'>
          <div className='container'>
            <div className='row justify-content-center'>
              <div className='col-10'>
              { this.state.data.map((q, i) => <ItemQuiz {...q} key={i} />) }
              </div>
            </div>
          </div>
        </div>
      )
    }
  }
}

const ItemQuiz = (q) => {
  return (
    <div className='col-md-10 col-12 '>
      <div className='card mb-10 shadow-sm col-12'>
        <div className='card-body'>
          <h5 className='card-title'><Link to={`/quiz/${q._id}`}>{q.name}</Link></h5>
          <p className='tags'>
            {q.keywords.map((k, indexKeywords) => <span className='tag is-rounded' key={indexKeywords}> {k} </span>)}
          </p>
          <div className='card-title'><Link to={`/putQuiz/${q._id}`}>Modifier</Link></div>
        </div>
      </div>
    </div>
  )
}

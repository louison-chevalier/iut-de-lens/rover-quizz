import React, { Component, Fragment } from 'react'
import { HTTP_SERVER_PORT } from '../constants'
import { Link } from 'react-router-dom'
import axios from 'axios'
import _ from 'lodash'
import 'bulma'

export default class Quiz extends Component {
  constructor (props) {
    super(props)
    this.state = {
      data: [],
      current: 0,
      currAnswer: [],
      answers: []
    }
    this.loadData()
  }

  async loadData () {
    const quiz = (await axios.get(`${HTTP_SERVER_PORT}/quizzes/` + this.props.match.params.id)).data
    this.setState({ data: quiz })
  }

  handleCheck = (e) => {
    let currAnswer = this.state.currAnswer
    if (e.target.checked === true) {
      currAnswer.push(+e.target.value)
    } else {
      // supprime la reponse décochée
      currAnswer = currAnswer.filter(rep => rep !== +e.target.value)
    }
    console.log(currAnswer)
    this.setState({ currAnswer })
  }

  submitQuestion = (e) => {
    e.preventDefault()
    let answers = this.state.answers
    let currAnswer = this.state.currAnswer
    currAnswer.sort((a, b) => a - b)
    answers.push(currAnswer)

    this.setState({ currAnswer: [], answers, current: this.state.current + 1 })
  }

  render () {
    if (this.state.data === undefined || this.state.data.length === 0) {
      return (
        <Fragment>
          <div>Load data...</div>
        </Fragment>
      )
    }

    let quiz = this.state.data
    // Si toutes les réponses sons faites
    if (quiz.questions.length === this.state.current) {
      let max = quiz.questions.reduce((acc, q) => {
        acc += q.points
        return acc
      }, 0)
      let score = quiz.questions.reduce((acc, q, index) => {
        q.solutions = q.solutions.map(sol => +sol)
        if (_.isEqual(q.solutions, this.state.answers[index])) {
          acc += q.points
        }
        return acc
      }, 0)

      return (
        <div className='container'>
          <div className='row justify-content-center align-items-center' style={{ height: 80 + 'vh' }}>
            <div className='col-md-4 col-sm-1 '>
              <h4 className='h4 mb-3 font-weight-normal text-center'>Bien joué ! <br /></h4>
              <h3 className='h3 mb-3 font-weight-normal text-center'> Score : {score} / {max}</h3>
              <Link to={`/quizzes`}><button className='btn btn-primary text-center' >Accueil</button></Link>
            </div>
          </div>
        </div>
      )
    }
    let question = quiz.questions[this.state.current]
    return (
      <div className='container'>
        <div className='row justify-content-center align-items-center' style={{ height: 80 + 'vh' }}>
          <div className='col-md-4 col-sm-1 '>
            <h3 className='h3 mb-3 font-weight-normal text-center'>{quiz.name}</h3>
            <h4 className='h4 mb-3 font-weight-normal text-center'>{question.question}</h4>
            <form className='form-check mb-6' method='post' onSubmit={this.submitQuestion}>
              <ol className='lead' name='reponses'>{question.txtAnswers.map((q, index) => {
                return (
                  <div key={question + '-reponses-' + index}>
                    <label className='checkbox'>
                      <input key={`reponse-${index}-${q}`} className='form-check-input' type='checkbox' onChange={this.handleCheck} value={index + 1} /> {q}
                    </label>
                  </div>
                )
              })}
              </ol>
              <button className='btn btn-lg btn-secondary' type='submit' >Valider</button>
            </form>
          </div>
        </div>
      </div>
    )
  }
}
